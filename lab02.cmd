@echo off

REM Перевірка кількості переданих аргументів
if "%~6"=="" (
    echo Потрібно ввести 6 аргументів: lab.log path_to_file xx.exe path_to_archive ip_address log_size
    exit /b
)

REM Отримання переданих аргументів
set "LogName=%1"
set "PathToFile=%2"
set "ProcessName=%3"
set "PathToArchive=%4"
set "IPAddress=%5"
set "LogSize=%6"

REM Перевірка наявності файлу log
if not exist %LogName% (
    echo %DATE% %TIME% > %LogName%
    echo Файл з ім’ям %LogName% відкрито або створено >> %LogName%
) else (
    echo Файл %LogName% вже існує. Записуємо в нього
)

REM Дописування у log файл
echo %DATE% %TIME% >> %LogName%
echo Файл з ім’ям %LogName% відкрито або створено >> %LogName%

REM Отримання часу з NTP серверу
w32tm /stripchart /computer:%IPAddress% /samples:1 /dataonly >> %LogName%

REM Встановлення отриманого часу як поточного
w32tm /resync
REM Записати оновлений час у log.
echo %DATE% %TIME% Оновлений час отриманий з NTP серверу >> %LogName%

REM Виводить у log список усіх запущених процесів.
tasklist >> %LogName%
echo Виведений список усіх запущених процесів у %LogName%

REM Завершує процес, з ім’ям Аргумент3. Інформує в log.
taskkill /F /IM %ProcessName%
echo Завершено процес з ім'ям %ProcessName% >> %LogName%

REM Підрахунок файлів до видалення
for /f %%i in ('dir /b /a-d "%PathToFile%\*.tmp" ^| find /c /v ""') do set BeforeCount=%%i
for /f %%i in ('dir /b /a-d "%PathToFile%\temp*.*" ^| find /c /v ""') do set /a BeforeCount+=%%i

REM Видалення файлів
del /Q %PathToFile%\temp*.* 2>nul
del /Q %PathToFile%\*.tmp 2>nul

REM Підрахунок файлів після видалення
for /f %%i in ('dir /b /a-d "%PathToFile%\*.tmp" ^| find /c /v ""') do set AfterCount=%%i
for /f %%i in ('dir /b /a-d "%PathToFile%\temp*.*" ^| find /c /v ""') do set /a AfterCount+=%%i

REM Виведення кількості видалених файлів у лог
set /a CountDeletedFiles = BeforeCount - AfterCount
echo Кількість видалених файлів: %CountDeletedFiles% >> %LogName%

REM Отримання поточної дати та часу через PowerShell
for /F %%I in ('powershell -Command "Get-Date -Format 'yyyy-MM-dd_HH-mm-ss'"') do set "DateTime=%%I"
REM Стискання файлів у архів .zip за поточну дату та час
powershell Compress-Archive -Path "%PathToFile%\*" -DestinationPath "%PathToArchive%\%DateTime%.zip"

REM Отримання дати за минулий день
for /f %%I in ('powershell -command "(Get-Date).AddDays(-1).ToString('yyyy-MM-dd')"') do set yesterdayDate=%%I

REM Перевірка наявності архіву за минулий день
IF EXIST "%PathToArchive%\%yesterdayDate%.zip" (
    echo Архів за минулий день знайдений (%yesterdayDate%.zip) >> %LogName%
) ELSE (
    echo Архів за минулий день не знайдений (%yesterdayDate%.zip) >> %LogName%
)

REM Інформація про виконані дії
echo Стисли файли у архів. Перемістили ахрів у папку за шляхом Аргумент4. Перевірили наявність архіву за минулий день: Інформацію про виконані дії записано у log файл >> %LogName%

REM Перевірка та видалення архівів старіших за 30 днів
forfiles /p %PathToArchive% /s /m *.zip /d -30 /c "cmd /c del @path" 2>nul
echo Видалено архіви, старші 30 днів >> %LogName%

REM Перевірка підключення до Internet
ping 8.8.8.8
if %errorlevel% equ 0 (
    echo Є підключення до Інтернету >> %LogName%
) else (
    echo Немає підключення до Інтернету >> %LogName%
)

REM Перевірити чи є в локальній мережі комп’ютер з IP аргумент5, та якщо є, то завершити його роботу. Інформувати в log.
ping %IPAddress%
if %errorlevel% neq 1 (
    taskkill /S %IPAddress% /IM %ProcessName%.exe /F
    echo Комп'ютер з IP-адресою %IPAddress% знаходиться у мережі. Завершено процес %ProcessName% на цій стації >> %LogName%
) else (
    echo Комп'ютер з IP-адресою %IPAddress% не знайдено у мережі >> %LogName%
)

REM Отримує список комп’ютерів в мережі та записує отриману інформацію у log.
powershell -Command "Get-WmiObject -Class Win32_ComputerSystem | Out-File -FilePath 'C:\Users\pavlo\source\repos\sysproglab02\lab2.log' -Append"

REM Отримання розміру файлу
for %%I in ("%LogName%") do set LogSizeToCheck=%%~zI

REM Порівняння розміру файлу з переданим значенням
if %LogSizeToCheck% gtr %LogSize% (
    echo Розмір log файлу перевищує вказане значення >> %LogName%
) else (
    echo Розмір log файлу в межах допустимого >> %LogName%
)

REM Отримання інформації про диски та запис в log файл
wmic logicaldisk get caption,description,filesystem,freespace,size,volumename >> %LogName%

REM Отримання поточної дати та часу
for /f "tokens=2 delims==" %%I in ('wmic os get localdatetime /value') do set datetime=%%I
set datetime=%datetime:~0,4%-%datetime:~4,2%-%datetime:~6,2%_%datetime:~8,2%-%datetime:~10,2%-%datetime:~12,2%

REM Запуск команди systeminfo та перенаправлення результату у новий файл
systeminfo > "systeminfo_%datetime%.txt"